package fr.eurecom.android.test;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity2 extends Activity implements WebSocketService.OnServiceListener {

    private static final String TAG = "MainActivity2";

    private TextView msg_field;

    WebSocketService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        msg_field = findViewById(R.id.display_username);

        Log.i(TAG, "onCreate");

        Intent intent = new Intent(this, WebSocketService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);

        Button createGameButton = (Button) findViewById(R.id.create_game_button);
        createGameButton.setOnClickListener(handler);
        Button joinViewButton = (Button) findViewById(R.id.join_view_button);
        joinViewButton.setOnClickListener(handler);
    }

    View.OnClickListener handler = new View.OnClickListener() {
        public void onClick(View v) {
            Log.i("handler called, Vid is", String.valueOf(v.getId()));
            if (v.getId() == R.id.create_game_button) {
                try {
                    ws_send("create_game", "0");
                    Log.i(TAG, "Start Game");
                } catch (JSONException e) {
                    Log.i(TAG, "Not send");
                }
            }
            if (v.getId() == R.id.join_view_button) {
                try {
                    ws_send("add_bot","0");
                    Log.i(TAG, "Add bot 0");
                } catch (JSONException e) {
                    Log.i(TAG, "Not send");
                }
            }
        }
    };

    public void ws_send(String type, String data) throws JSONException {
        Log.i(TAG, "ws_send " + type + " and " + data);
        JSONObject obj = new JSONObject();
        switch(type){
            case "create_game":
                obj.put("type", "create game");
                obj.put("game_id", data);
                break;
            case "join":
                obj.put("type", "join");
                obj.put("game_id", data);
                break;
            case "add_bot":
                obj.put("type", "add_bot");
                obj.put("game_id", data);
                break;
            case "start_game":
                obj.put("type", "start_game");
                obj.put("game_id", data);
                break;
            case "send_play":
                obj.put("type", "send_play");
                obj.put("play", data);
//                gameView.resetValidCards();
                break;
            default:
                Log.e("send error", "wrong type provided");
                break;

        }
        String jsonText = obj.toString();
        mService.send(jsonText);
    }

    @Override
    public void onDataReceived(String data) {
        Log.i(TAG, "onDataReceived");
        display_msg(data);
    }

    private void display_msg(final String text) {
        Log.i(TAG, "display_msg");
        runOnUiThread(() -> msg_field.setText(msg_field.getText().toString() + "\n" + text));
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.i(TAG, "onServiceConnected");
            mService = ((WebSocketService.LocalBinder) iBinder).getService();
            mService.setOnServiceListener(MainActivity2.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }

    };
}