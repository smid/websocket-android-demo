package fr.eurecom.android.test;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class WebSocketService extends IntentService {

    private static final String TAG = "WebSocketService";

    private static final String SERVER = "10.0.2.2";  // Emulator IP
    private static final int PORT = 8000;

    private static final String EXTRA_MESSAGE = "fr.eurecom.marias_client.extra.MESSAGE";
    private static final String USERNAME = "USERNAME";

    private OnServiceListener mOnServiceListener = null;

    private WebSocket ws;
    private OkHttpClient client;
    private String message = null;
    private String username;

    private final class WebSocketClient extends WebSocketListener {

        @Override
        public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
            Log.i(TAG, "WebSocket connection established");
        }

        @Override
        public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
            message = text;
            Log.i(TAG, message);
            onDataReceived(text);
        }

        @Override
        public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
        }

        @Override
        public void onFailure(@NotNull WebSocket webSocket,
                              @NotNull Throwable t,
                              @Nullable Response response) {
            Log.e(TAG,"Error : " + t.getMessage(), t);
        }

        @Override
        public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
            webSocket.close(code, reason);
            Log.i(TAG, "WebSocket connection closed");
        }
    }

    public WebSocketService() {
        super("WebSocketService");
    }

    private void connect() {
        Request request = new Request.Builder().url(
                "ws://" + SERVER + ":" + PORT + "/ws/" + this.username).build();
        WebSocketClient listener = new WebSocketClient();
        ws = client.newWebSocket(request, listener);
        client.dispatcher().executorService().shutdown();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "service created");
        client = new OkHttpClient();
//        connect();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Log.i(TAG, "service connected to an Intent");
        }
    }

    public interface OnServiceListener{
        public void onDataReceived(String data);
    }

    public void setOnServiceListener(OnServiceListener serviceListener){
        mOnServiceListener = serviceListener;
    }

    public void onDataReceived(String data) {
        Log.i(TAG, "onDataReceived");
        if(mOnServiceListener != null){
            Log.i(TAG, "mOnServiceListener");
            mOnServiceListener.onDataReceived(data);
        }
    }

    private final IBinder binder = new LocalBinder();


    public class LocalBinder extends Binder {
        WebSocketService getService() {
            // Return this instance of LocalService so clients can call public methods
            return WebSocketService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "service bound");
//        ws.send("service bound");
        String username = null;
        username = intent.getStringExtra(USERNAME);
        if (username != null && this.username == null) {
            Log.i(TAG, "username was passed to a service");
            this.username = username;
            connect();
        }
        Log.i(TAG, "service bound as " + this.username);
        return binder;
    }

    public void send(String msg) {
        Log.i(TAG, "Send: " + msg);
        ws.send(msg);
    }

}
